from Repositorio.InterfaceRepositorio import InterfaceRepositorio
from Modelos.Mesa import Mesa
from bson import ObjectId

class RepositorioMesa(InterfaceRepositorio[Mesa]):
	def OrdenVotantes2(self):

		query1 = {

			"$sort": {"$cantidad_inscritos:1"}

			}

		pipeline = [query1]
		return self.queryAggregation(pipeline)